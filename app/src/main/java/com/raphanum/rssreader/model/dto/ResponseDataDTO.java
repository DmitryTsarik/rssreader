package com.raphanum.rssreader.model.dto;

import java.util.List;

public class ResponseDataDTO {
    private List<ChannelDTO> entries;

    public List<ChannelDTO> getEntries() {
        return entries;
    }
}

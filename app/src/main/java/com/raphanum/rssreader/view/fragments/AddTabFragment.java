package com.raphanum.rssreader.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raphanum.rssreader.R;

public class AddTabFragment extends AbstractTabFragment {

    public static AddTabFragment newInstance(Context context) {
        AddTabFragment fragment = new AddTabFragment();
        fragment.setTitle(context.getString(R.string.tab_title_add));
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_tab, container, false);
    }
}

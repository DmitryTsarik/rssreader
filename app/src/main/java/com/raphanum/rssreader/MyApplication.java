package com.raphanum.rssreader;

import android.app.Application;

import com.raphanum.rssreader.di.AppComponent;
import com.raphanum.rssreader.di.DaggerAppComponent;
import com.raphanum.rssreader.di.NetworkModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyApplication extends Application {

    private static AppComponent component;

    public static AppComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder()
                .networkModule(new NetworkModule(this))
                .build();

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }
}

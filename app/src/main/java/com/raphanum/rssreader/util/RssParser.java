package com.raphanum.rssreader.util;

import com.raphanum.rssreader.model.realm.Channel;
import com.raphanum.rssreader.model.realm.RssItem;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class RssParser {
    private Document xmlDocument;
    private List<RssItem> items;
    private Channel channel;

    public RssParser() {
        channel = new Channel();
        items = new ArrayList<>();
    }

    public void parse(String channelUrl, String xml) {
        try {
            xmlDocument = DocumentBuilderFactory
                    .newInstance()
                    .newDocumentBuilder()
                    .parse(new InputSource(new StringReader(xml)));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        if (xmlDocument != null) {
            createChannel(channelUrl);
            createItemList();
        }
    }

    public Channel getChannel() {
        return channel;
    }

    public List<RssItem> getItems() {
        return items;
    }

    private void createChannel(String url) {
        channel.setUrl(url);
        Node channelNode = xmlDocument.getElementsByTagName(Constants.RSS_CHANNEL_TAG).item(0);
        channel.setTitle(getTextContentByTagName(channelNode, Constants.RSS_TITLE_TAG));
        channel.setImageUrl(findChannelImageUrl(channelNode));
    }

    private String findChannelImageUrl(Node channelNode) {
        Node image = findChildByTagName(channelNode, Constants.RSS_IMAGE_TAG);
        Node url = findChildByTagName(image, Constants.RSS_LINK_TAG);
        return getTagContent(url) + Constants.FAVICON_PATH;
    }

    private Node findChildByTagName(Node node, String tagName) {
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i).getNodeName().equals(tagName)) {
                return nodeList.item(i);
            }
        }
        return null;
    }

    private String getTagContent(Node node) {
        return node.getTextContent().replaceAll("  ", "").replace("\n","");
    }

    private void createItemList() {
        if (xmlDocument != null) {
            NodeList nodeList = xmlDocument.getElementsByTagName(Constants.RSS_ITEM_TAG);
            for (int i = 0; i < nodeList.getLength(); i++) {
                items.add(createItem(nodeList.item(i)));
            }
        }
    }

    private RssItem createItem(Node itemNode) {
        RssItem item = new RssItem();
        item.setTitle(getTextContentByTagName(itemNode, Constants.RSS_TITLE_TAG));
        item.setPubDate(getPubDate(itemNode));
        item.setDescription(getTextContentByTagName(itemNode, Constants.RSS_DESCRIPTION_TAG));
        item.setUrl(getTextContentByTagName(itemNode, Constants.RSS_LINK_TAG));
        item.setImageUrl(getItemImage(itemNode));
        item.setChannelUrl(channel.getUrl());
        item.setChannelImageUrl(channel.getImageUrl());
        item.setChannelTitle(channel.getTitle());
        return item;
    }

    private Date getPubDate(Node node) {
        DateFormat formatter = new SimpleDateFormat(Constants.RSS_PUB_DATE_FORMAT, Locale.ENGLISH);
        Date date = new Date();
        try {
            date = formatter.parse(getTextContentByTagName(node, Constants.RSS_PUB_DATE_TAG));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private String getItemImage(Node itemNode) {
        NodeList nodes = itemNode.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            String nodeName = nodes.item(i).getNodeName();
            if (nodeName.equals(Constants.RSS_ENCLOSURE_TAG) || nodeName.contains(Constants.RSS_MEDIA_TAG)) {
                return nodes.item(i).getAttributes().getNamedItem(Constants.RSS_URL_TAG).getTextContent();
            }
        }
        return "";
    }

    private String getTextContentByTagName(Node node, String tagName) {
        Node child = findChildByTagName(node, tagName);
        if (child != null) {
            return getTagContent(child);
        }
        return "";
    }
}

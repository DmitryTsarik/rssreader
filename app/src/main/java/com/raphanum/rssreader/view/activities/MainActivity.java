package com.raphanum.rssreader.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.raphanum.rssreader.R;
import com.raphanum.rssreader.di.DaggerMainViewComponent;
import com.raphanum.rssreader.di.MainViewComponent;
import com.raphanum.rssreader.di.MainViewModule;
import com.raphanum.rssreader.model.realm.RssItem;
import com.raphanum.rssreader.presenter.MainPresenter;
import com.raphanum.rssreader.util.Constants;
import com.raphanum.rssreader.view.MainView;
import com.raphanum.rssreader.view.adpters.RssItemListAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainView {

    @Inject
    protected MainPresenter presenter;
    private MainViewComponent viewComponent;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private RecyclerView recyclerView;
    private String state;
    private static final int REQUEST_CODE = 5678;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initDrawerAndActionBar();
        initRecyclerView();
        initViewComponent();

        if (savedInstanceState != null) {
            state = savedInstanceState.getString(Constants.MAIN_ACTIVITY_STATE_KEY);
        } else {
            state = Constants.ALL_NEWS_STATE;
        }
        presenter.onCreate(state);

        initNavigationView();
        initSwipeRefreshLayout();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        drawer.removeDrawerListener(toggle);
        presenter.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Constants.MAIN_ACTIVITY_STATE_KEY, state);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            presenter.onCreate(data.getStringExtra(Constants.NEWS_LIST_TRANSPORT_KEY));
            recyclerView.scrollToPosition(data.getIntExtra(Constants.NEWS_ITEM_POSITION_KEY, 0));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.action_delete_news:
                presenter.onClearNewsClick();
                return true;
            case R.id.action_delete_all:
                presenter.onClearAllClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        recyclerView.scrollToPosition(0);
        switch (item.getItemId()) {
            case R.id.nav_add_channel:
                presenter.onAddChannelClick();
                break;
            case R.id.nav_all_news:
                presenter.onAllNewsClick();
                state = Constants.ALL_NEWS_STATE;
                break;
            case R.id.nav_favorites:
                presenter.onFavoritesClick();
                state = Constants.FAVORITES_STATE;
                break;
            case R.id.nav_about:
                presenter.onAboutClick();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initDrawerAndActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void initNavigationView() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_all_news);
    }

    private void initSwipeRefreshLayout() {
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.content_swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.onRefresh());
    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.feed_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RssItemListAdapter adapter = new RssItemListAdapter(new ArrayList<>());
        adapter.setOnLoadImageListener((view, url) -> {
            // encapsulate this code into separate manager class
            GlideDrawableImageViewTarget target = new GlideDrawableImageViewTarget(view);
            Glide.with(this)
                    .load(url)
                    .placeholder(R.mipmap.feed)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(target);
        });
        adapter.setOnItemClickListener(new RssItemListAdapter.OnItemClickListener() {
            @Override
            public void onFavoriteClick(int position) {
                presenter.onItemFavoriteClick(position);
            }

            @Override
            public void onClick(int position) {
                presenter.onItemClick(position);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void initViewComponent() {
        if (viewComponent == null) {
            viewComponent = DaggerMainViewComponent.builder()
                    .mainViewModule(new MainViewModule(this))
                    .build();
        }
        viewComponent.inject(this);
    }

    private void setRefreshing(boolean refreshing) {
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.content_swipe_refresh);
        swipeRefreshLayout.setRefreshing(refreshing);
    }

    @Override
    public void showLoading() {
        setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        setRefreshing(false);
    }

    @Override
    public void showNews(List<RssItem> list) {
        findViewById(R.id.empty_list_layer).setVisibility(View.GONE);
        ((RssItemListAdapter)recyclerView.getAdapter()).setRssItemList(list);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showEmpty() {
        recyclerView.setVisibility(View.GONE);
        findViewById(R.id.empty_list_layer).setVisibility(View.VISIBLE);
    }

    @Override
    public void showItem(int position) {
        Intent intent = new Intent(this, NewsActivity.class);
        intent.putExtra(Constants.NEWS_LIST_TRANSPORT_KEY, state);
        intent.putExtra(Constants.NEWS_ITEM_POSITION_KEY, position);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    public void showAbout() {
        startActivity(new Intent(this, AboutActivity.class));
    }

    @Override
    public void showAddChannel() {
        startActivity(new Intent(this, ChannelsActivity.class));
    }

    @Override
    public void showError() {
        Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
    }
}

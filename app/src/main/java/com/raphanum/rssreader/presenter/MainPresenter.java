package com.raphanum.rssreader.presenter;

import android.util.Log;

import com.raphanum.rssreader.MyApplication;
import com.raphanum.rssreader.model.realm.RssItem;
import com.raphanum.rssreader.util.Constants;
import com.raphanum.rssreader.util.DatabaseManager;
import com.raphanum.rssreader.util.NetworkManager;
import com.raphanum.rssreader.util.RssParser;
import com.raphanum.rssreader.view.MainView;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;

public class MainPresenter extends BasePresenter {
    private MainView view;
    private RssParser parser;
    private List<RssItem> items;
    @Inject
    protected NetworkManager networkManager;
    @Inject
    protected DatabaseManager databaseManager;

    @Inject
    public MainPresenter() {
    }

    public MainPresenter(MainView view) {
        MyApplication.getComponent().inject(this);
        this.view = view;
        parser = new RssParser();
        networkManager.setOnResponseListener(new NetworkManager.OnResponseListener() {
            @Override
            public void onResponse(String url, String response) {
                Log.v("Time Log", "Network response: " + System.currentTimeMillis());
                view.hideLoading();
                parser.parse(url, response);
                Log.v("Time Log", "Parsed: " + System.currentTimeMillis());
                databaseManager.save(parser.getItems());
                Log.v("Time Log", "Saved to db: " + System.currentTimeMillis());
            }

            @Override
            public void onError() {
                view.hideLoading();
            }
        });
    }

    @Override
    public void onCreate(String state) {
        if (state.equals(Constants.FAVORITES_STATE)) {
            onFavoritesClick();
        } else {
            onAllNewsClick();
        }
    }

    public void onRefresh() {
        view.showLoading();
        loadFeeds();
    }

    public void onItemClick(int position) {
        view.showItem(position);
    }

    public void onAllNewsClick() {
        Subscription subscription = model.getAllItems()
                .subscribe(list -> {
                    if (list.size() > 0) {
                        items = list;
                        view.showNews(items);
                    } else {
                        view.showEmpty();
                    }
                },
                        Throwable::printStackTrace);
        setSubscription(subscription);
    }

    public void onFavoritesClick() {
        Subscription subscription = model.getFavorites()
                .subscribe(list -> {
                    if (list.size() > 0) {
                        items = list;
                        view.showNews(list);
                    } else {
                        view.showEmpty();
                    }

                },
                        Throwable::printStackTrace);
        setSubscription(subscription);
    }

    public void onAboutClick() {
        view.showAbout();
    }

    public void onAddChannelClick() {
        view.showAddChannel();
    }

    private void loadFeeds() {
        Log.v("Time Log", "Load feeds: " + System.currentTimeMillis());
        model.getFakeChannelList() //model.getChannelList();
                .flatMap(Observable::from)
                .subscribe(channel -> networkManager.loadFeed(channel.getUrl()))
                .unsubscribe(); // load data from url
    }

    public void onClearNewsClick() {
        databaseManager.deleteAllNews();
    }

    public void onClearAllClick() {
        databaseManager.deleteAll();
    }

    public void onItemFavoriteClick(int position) {
        RssItem item = items.get(position);
        databaseManager.updateFavorite(item, !item.isFavorite());
    }
}

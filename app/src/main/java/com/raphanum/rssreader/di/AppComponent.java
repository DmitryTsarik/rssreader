package com.raphanum.rssreader.di;

import com.raphanum.rssreader.presenter.BasePresenter;
import com.raphanum.rssreader.presenter.FindChannelPresenter;
import com.raphanum.rssreader.presenter.MainPresenter;
import com.raphanum.rssreader.presenter.NewsPresenter;
import com.raphanum.rssreader.util.NetworkManager;
import com.raphanum.rssreader.view.fragments.FindTabFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {PresenterModule.class, NetworkModule.class})
public interface AppComponent {

    void inject(BasePresenter basePresenter);

    void inject(MainPresenter mainPresenter);

    void inject(NewsPresenter newsPresenter);

    void inject(FindChannelPresenter findChannelPresenter);

    void inject(NetworkManager networkManager);
}

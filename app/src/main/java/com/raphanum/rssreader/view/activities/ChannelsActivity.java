package com.raphanum.rssreader.view.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.raphanum.rssreader.R;
import com.raphanum.rssreader.view.adpters.ChannelsFragmentPagerAdapter;

public class ChannelsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channels);

        Toolbar toolbar = (Toolbar) findViewById(R.id.channel_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ((NestedScrollView) findViewById(R.id.channels_scroll_view)).setFillViewport(true);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.channels_tab_layout);
        ViewPager viewPager = (ViewPager) findViewById(R.id.channels_view_pager);
        viewPager.setAdapter(new ChannelsFragmentPagerAdapter(this, getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
    }
}

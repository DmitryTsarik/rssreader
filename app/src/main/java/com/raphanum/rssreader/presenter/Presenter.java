package com.raphanum.rssreader.presenter;

public interface Presenter {

    void onCreate(String state);

    void onDestroy();
}

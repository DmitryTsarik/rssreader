package com.raphanum.rssreader.di;

import com.raphanum.rssreader.presenter.NewsPresenter;
import com.raphanum.rssreader.view.NewsView;

import dagger.Module;
import dagger.Provides;

@Module
public class NewsViewModule {

    private NewsView view;

    public NewsViewModule(NewsView view) {
        this.view = view;
    }

    @Provides
    NewsPresenter provideNewsPresenter() {
        return new NewsPresenter(view);
    }
}

package com.raphanum.rssreader.view;

import com.raphanum.rssreader.model.realm.RssItem;

import java.util.List;

public interface NewsView {

    void setNewsList(List<RssItem> items, int currentPosition);

    void setFavoriteIcon(boolean isFavorite);
}

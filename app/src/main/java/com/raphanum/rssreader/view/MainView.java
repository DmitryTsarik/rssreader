package com.raphanum.rssreader.view;

import com.raphanum.rssreader.model.realm.RssItem;

import java.util.List;

public interface MainView {

    void showLoading();

    void hideLoading();

    void showNews(List<RssItem> list);

    void showEmpty();

    void showItem(int position);

    void showAbout();

    void showAddChannel();

    void showError();
}

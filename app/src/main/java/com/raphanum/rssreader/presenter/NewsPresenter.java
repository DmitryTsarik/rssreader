package com.raphanum.rssreader.presenter;

import com.raphanum.rssreader.MyApplication;
import com.raphanum.rssreader.model.realm.RssItem;
import com.raphanum.rssreader.util.Constants;
import com.raphanum.rssreader.util.DatabaseManager;
import com.raphanum.rssreader.view.NewsView;

import java.util.List;

import javax.inject.Inject;

import rx.Subscription;

public class NewsPresenter extends BasePresenter {

    private NewsView view;
    private List<RssItem> items;
    private int position;
    @Inject
    protected DatabaseManager databaseManager;

    @Inject
    public NewsPresenter() {}

    public NewsPresenter(NewsView view) {
        MyApplication.getComponent().inject(this);
        this.view = view;
    }

    @Override
    public void onCreate(String state) {
        if (state.equals(Constants.FAVORITES_STATE)) {
            subscribeOnFavorites();
        } else {
            subscribeOnAllNews();
        }
    }

    public void onFavoriteClick() {
        RssItem item = items.get(position);
        view.setFavoriteIcon(!item.isFavorite());
        databaseManager.updateFavorite(item, !item.isFavorite());
    }

    public void onOpenInBrowserClick() {

    }

    public void setItemPosition(int position) {
        this.position = position;
    }

    private void subscribeOnAllNews() {
        Subscription subscription = model.getAllItems()
                .subscribe(list -> {
                    items = list;
                    getItems();
                    setItemIsFavorite();
                },
                        Throwable::printStackTrace);
        setSubscription(subscription);
    }

    private void subscribeOnFavorites() {
        Subscription subscription = model.getFavorites()
                .subscribe(list -> {
                    items = list;
                    getItems();
                    setItemIsFavorite();
                },
                        Throwable::printStackTrace);
        setSubscription(subscription);
    }

    private void getItems() {
        view.setNewsList(items, position);
    }

    public void setItemIsFavorite() {
        view.setFavoriteIcon(items.get(position).isFavorite());
    }
}

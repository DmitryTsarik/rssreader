package com.raphanum.rssreader.di;

import com.raphanum.rssreader.view.activities.MainActivity;
import com.raphanum.rssreader.view.activities.NewsActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {MainViewModule.class})
public interface MainViewComponent {

    void inject(MainActivity activity);
}

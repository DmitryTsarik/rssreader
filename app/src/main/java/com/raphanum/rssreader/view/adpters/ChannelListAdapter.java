package com.raphanum.rssreader.view.adpters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.raphanum.rssreader.R;
import com.raphanum.rssreader.model.dto.ChannelDTO;

import java.util.ArrayList;
import java.util.List;

public class ChannelListAdapter extends RecyclerView.Adapter<ChannelListAdapter.ChannelViewHolder> {

    private List<ChannelDTO> channels;

    public ChannelListAdapter() {
        channels = new ArrayList<>();
    }

    @Override
    public ChannelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.channel_item, parent, false);
        return new ChannelViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ChannelViewHolder holder, int position) {
        ChannelDTO channel = channels.get(position);
        holder.title.setText(channel.getTitle().replaceAll("<.*?>", ""));
        holder.url.setText(channel.getUrl());
    }

    @Override
    public int getItemCount() {
        if (channels != null) {
            return channels.size();
        }
        return 0;
    }

    public static class ChannelViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView url;

        public ChannelViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.channel_item_title);
            url = (TextView) itemView.findViewById(R.id.channel_item_url);
        }
    }

    public void setChannelList(List<ChannelDTO> list) {
        channels = list;
        notifyDataSetChanged();
    }
}

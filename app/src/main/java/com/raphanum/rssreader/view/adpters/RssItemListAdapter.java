package com.raphanum.rssreader.view.adpters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.raphanum.rssreader.R;
import com.raphanum.rssreader.model.realm.RssItem;
import com.raphanum.rssreader.util.Constants;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class RssItemListAdapter extends RecyclerView.Adapter<RssItemListAdapter.RssItemViewHolder> {

    private List<RssItem> rssItemList;
    private OnLoadImageListener onLoadImageListener;
    private OnItemClickListener onItemClickListener;

    public RssItemListAdapter(List<RssItem> list) {
        rssItemList = list;
    }

    public void setRssItemList(List<RssItem> list) {
        rssItemList = list;
        notifyDataSetChanged();
    }

    @Override
    public RssItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rss_item_layout, parent, false);
        return new RssItemViewHolder(itemView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(RssItemViewHolder holder, int position) {
        if (rssItemList != null) {
            holder.position = position;
            holder.title.setText(rssItemList.get(position).getTitle());
            holder.pubDate.setText(getPubDate(position));
            onLoadImageListener.loadImage(holder.channelImage, rssItemList.get(position).getChannelImageUrl());
            if (rssItemList.get(position).isFavorite()) {
                holder.favoriteView.setImageResource(R.drawable.ic_star);
            } else {
                holder.favoriteView.setImageResource(R.drawable.ic_star_border);
            }
        }
    }

    private String getPubDate(int position) {
        SimpleDateFormat outDateFormat = new SimpleDateFormat(Constants.DISPLAY_PUB_DATE_FORMAT, Locale.getDefault());
        return outDateFormat.format(rssItemList.get(position).getPubDate());
    }

    @Override
    public int getItemCount() {
        if (rssItemList != null) {
            return rssItemList.size();
        }
        return 0;
    }

    static public class RssItemViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView pubDate;
        private ImageView channelImage;
        private ImageView favoriteView;
        private int position;
        private View view;

        public RssItemViewHolder(View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.rss_item_title);
            pubDate = (TextView) itemView.findViewById(R.id.rss_item_pub_date);
            channelImage = (ImageView) itemView.findViewById(R.id.rss_item_channel_icon);
            favoriteView = (ImageView) itemView.findViewById(R.id.favorite_icon);
            view = itemView.findViewById(R.id.item_view);

            favoriteView.setOnClickListener(view -> onItemClickListener.onFavoriteClick(position));
            view.setOnClickListener(view1 -> onItemClickListener.onClick(position));
        }
    }

    public interface OnItemClickListener {
        void onFavoriteClick(int position);
        void onClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnLoadImageListener {
        void loadImage(ImageView view, String url);
    }

    public void setOnLoadImageListener(OnLoadImageListener onLoadImageListener) {
        this.onLoadImageListener = onLoadImageListener;
    }
}

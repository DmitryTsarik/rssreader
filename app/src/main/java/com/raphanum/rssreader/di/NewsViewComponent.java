package com.raphanum.rssreader.di;

import com.raphanum.rssreader.view.activities.NewsActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NewsViewModule.class})
public interface NewsViewComponent {

    void inject(NewsActivity activity);
}

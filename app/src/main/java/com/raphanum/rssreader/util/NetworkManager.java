package com.raphanum.rssreader.util;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.raphanum.rssreader.MyApplication;
import com.raphanum.rssreader.model.dto.ChannelDTO;
import com.raphanum.rssreader.model.dto.ResponseDTO;

import java.util.List;

import javax.inject.Inject;

public class NetworkManager {
    private OnResponseListener onResponseListener;
    private OnFindResponseListener onFindResponseListener;

    @Inject
    protected RequestQueue requestQueue;

    public NetworkManager() {
        MyApplication.getComponent().inject(this);
    }

    public void loadFeed(String url) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> onResponseListener.onResponse(url, response),
                error -> onResponseListener.onError());
        stringRequest.setShouldCache(true);
        requestQueue.add(stringRequest);
    }

    public void findChannel(String query) {
        String str = "https://ajax.googleapis.com/ajax/services/feed/find?v=1.0&num=200&q=" + query;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, str,
                response -> {
                    Gson gson = new GsonBuilder().create();
                    onFindResponseListener.onResponse(
                            gson.fromJson(response, ResponseDTO.class)
                                    .getResponseData()
                                    .getEntries());
                },
                error -> onFindResponseListener.onError());
        stringRequest.setShouldCache(true);
        requestQueue.add(stringRequest);
    }

    public interface OnResponseListener {
        void onResponse(String url, String response);

        void onError();
    }

    public interface OnFindResponseListener {
        void onResponse(List<ChannelDTO> list);

        void onError();
    }

    public void setOnResponseListener(OnResponseListener listener) {
        onResponseListener = listener;
    }

    public void setOnFindResponseListener(OnFindResponseListener onFindResponseListener) {
        this.onFindResponseListener = onFindResponseListener;
    }
}

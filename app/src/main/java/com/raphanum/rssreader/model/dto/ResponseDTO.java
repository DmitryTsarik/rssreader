package com.raphanum.rssreader.model.dto;

public class ResponseDTO {
    private ResponseDataDTO responseData;
    private int responseStatus;

    public ResponseDataDTO getResponseData() {
        return responseData;
    }

    public int getResponseStatus() {
        return responseStatus;
    }
}

package com.raphanum.rssreader.di;

import com.raphanum.rssreader.model.Model;
import com.raphanum.rssreader.model.ModelImpl;
import com.raphanum.rssreader.util.DatabaseManager;
import com.raphanum.rssreader.util.NetworkManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.subscriptions.CompositeSubscription;

@Module
public class PresenterModule {

    @Provides
    @Singleton
    Model provideModel() {
        return new ModelImpl();
    }

    @Provides
    @Singleton
    NetworkManager provideNetworkManager() {
        return new NetworkManager();
    }

    @Provides
    @Singleton
    DatabaseManager provideDatabaseManager() {
        return new DatabaseManager();
    }

    @Provides
    CompositeSubscription provideCompositeSubscription() {
        return new CompositeSubscription();
    }
}

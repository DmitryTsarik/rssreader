package com.raphanum.rssreader.util;

public class Constants {
    public static final String RSS_PUB_DATE_FORMAT = "EEE, dd MMM yyyy hh:mm:ss zzz";
    public static final String DISPLAY_PUB_DATE_FORMAT = "dd.MM.yyyy HH:mm";

    public static final String RSS_ITEM_TAG = "item";
    public static final String RSS_TITLE_TAG = "title";
    public static final String RSS_PUB_DATE_TAG = "pubDate";
    public static final String RSS_DESCRIPTION_TAG = "description";
    public static final String RSS_ENCLOSURE_TAG = "enclosure";
    public static final String RSS_MEDIA_TAG = "media";
    public static final String RSS_LINK_TAG = "link";
    public static final String RSS_URL_TAG = "url";
    public static final String RSS_IMAGE_TAG = "image";
    public static final String RSS_CHANNEL_TAG = "channel";

    public static final String FAVICON_PATH = "/favicon.ico";

    public static final String MAIN_ACTIVITY_STATE_KEY = "mainActivityStateKey";
    public static final String NEWS_LIST_TRANSPORT_KEY = "newsListTransportKey-";
    public static final String NEWS_ITEM_POSITION_KEY = "newsItemPositionKey";
    public static final String ALL_NEWS_STATE = "allNewsState";
    public static final String FAVORITES_STATE = "favoritesState";
}

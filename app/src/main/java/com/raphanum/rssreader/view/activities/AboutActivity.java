package com.raphanum.rssreader.view.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.raphanum.rssreader.BuildConfig;
import com.raphanum.rssreader.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView version = (TextView) findViewById(R.id.about_version);
        CharSequence str = version.getText() + " " + BuildConfig.VERSION_NAME;
        version.setText(str);
    }
}

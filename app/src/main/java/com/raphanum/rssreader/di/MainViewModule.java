package com.raphanum.rssreader.di;

import com.raphanum.rssreader.presenter.MainPresenter;
import com.raphanum.rssreader.view.MainView;

import dagger.Module;
import dagger.Provides;

@Module
public class MainViewModule {

    private MainView view;

    public MainViewModule(MainView view) {
        this.view = view;
    }

    @Provides
    MainPresenter provideMainPresenter() {
        return new MainPresenter(view);
    }
}

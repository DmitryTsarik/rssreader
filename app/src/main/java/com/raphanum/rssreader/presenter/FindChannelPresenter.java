package com.raphanum.rssreader.presenter;

import com.raphanum.rssreader.MyApplication;
import com.raphanum.rssreader.model.dto.ChannelDTO;
import com.raphanum.rssreader.util.NetworkManager;
import com.raphanum.rssreader.view.FindChannelView;

import java.util.List;

import javax.inject.Inject;

public class FindChannelPresenter {

    private FindChannelView view;
    @Inject
    protected NetworkManager networkManager;

    @Inject
    public FindChannelPresenter() {
    }

    public FindChannelPresenter(FindChannelView view) {
        MyApplication.getComponent().inject(this);
        this.view = view;
        setNetworkListener();
    }

    public void onFindClick(String query) {
        view.showLoading();
        networkManager.findChannel(query);
    }

    private void setNetworkListener() {
        networkManager.setOnFindResponseListener(new NetworkManager.OnFindResponseListener() {
            @Override
            public void onResponse(List<ChannelDTO> list) {
                if (list != null && list.size() > 0) {
                    view.hideLoading();
                    view.showChannelList(list);
                } else {
                    view.hideLoading();
                    view.showNotFound();
                }
            }

            @Override
            public void onError() {
                view.hideLoading();
                view.showNotFound();
            }
        });
    }
}

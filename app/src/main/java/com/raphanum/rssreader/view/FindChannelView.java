package com.raphanum.rssreader.view;

import com.raphanum.rssreader.model.dto.ChannelDTO;

import java.util.List;

public interface FindChannelView {

    void showChannelList(List<ChannelDTO> list);

    void showNotFound();

    void showLoading();

    void hideLoading();
}

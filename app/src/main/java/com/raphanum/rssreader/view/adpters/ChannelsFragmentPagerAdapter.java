package com.raphanum.rssreader.view.adpters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.raphanum.rssreader.view.fragments.AbstractTabFragment;
import com.raphanum.rssreader.view.fragments.AddTabFragment;
import com.raphanum.rssreader.view.fragments.ChangeTabFragment;
import com.raphanum.rssreader.view.fragments.FindTabFragment;

import java.util.ArrayList;
import java.util.List;

public class ChannelsFragmentPagerAdapter extends FragmentPagerAdapter {

    private List<AbstractTabFragment> tabs;

    public ChannelsFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        initTabs(context);
    }

    @Override
    public Fragment getItem(int position) {
        return tabs.get(position);
    }

    @Override
    public int getCount() {
        return tabs.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs.get(position).getTitle();
    }

    private void initTabs(Context context) {
        tabs = new ArrayList<>();
        tabs.add(FindTabFragment.newInstance(context));
        tabs.add(AddTabFragment.newInstance(context));
        tabs.add(ChangeTabFragment.newInstance(context));
    }
}

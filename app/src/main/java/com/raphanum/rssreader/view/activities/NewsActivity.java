package com.raphanum.rssreader.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.raphanum.rssreader.R;
import com.raphanum.rssreader.di.DaggerNewsViewComponent;
import com.raphanum.rssreader.di.NewsViewComponent;
import com.raphanum.rssreader.di.NewsViewModule;
import com.raphanum.rssreader.model.realm.RssItem;
import com.raphanum.rssreader.presenter.NewsPresenter;
import com.raphanum.rssreader.util.Constants;
import com.raphanum.rssreader.view.NewsView;
import com.raphanum.rssreader.view.adpters.NewsPagerAdapter;

import java.util.List;

import javax.inject.Inject;

public class NewsActivity extends AppCompatActivity implements NewsView {

    private NewsPagerAdapter newsPagerAdapter;
    private ViewPager viewPager;
    @Inject
    protected NewsPresenter presenter;
    private NewsViewComponent viewComponent;
    private ViewPager.OnPageChangeListener onPageChangeListener;
    private MenuItem favoriteItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        initViewComponent();
        initActionBar();
        initViewPager();
        initResultIntent();

        presenter.setItemPosition(getIntent().getIntExtra(Constants.NEWS_ITEM_POSITION_KEY, 0));
        presenter.onCreate(getIntent().getStringExtra(Constants.NEWS_LIST_TRANSPORT_KEY));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());
    }

    private void initResultIntent() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(Constants.NEWS_ITEM_POSITION_KEY, 0);
        resultIntent.putExtra(Constants.NEWS_LIST_TRANSPORT_KEY, getIntent().getStringExtra(Constants.NEWS_LIST_TRANSPORT_KEY));
        setResult(RESULT_OK, resultIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewPager.removeOnPageChangeListener(onPageChangeListener);
        presenter.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivity(RESULT_OK);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_news, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        favoriteItem = menu.findItem(R.id.action_favorite);
        presenter.setItemIsFavorite();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.action_favorite:
                presenter.onFavoriteClick();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setNewsList(List<RssItem> items, int currentPosition) {
        newsPagerAdapter.setItems(items);
        viewPager.setCurrentItem(currentPosition);
    }

    @Override
    public void setFavoriteIcon(boolean isFavorite) {
        if (favoriteItem != null && isFavorite) {
            favoriteItem.setIcon(getResources().getDrawable(R.drawable.ic_star));
        } else {
            favoriteItem.setIcon(getResources().getDrawable(R.drawable.ic_star_border_white));
        }
    }

    private void initViewComponent() {
        if (viewComponent == null) {
            viewComponent = DaggerNewsViewComponent.builder()
                    .newsViewModule(new NewsViewModule(this))
                    .build();
        }
        viewComponent.inject(this);
    }

    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initViewPager() {
        final String state = getIntent().getStringExtra(Constants.NEWS_LIST_TRANSPORT_KEY);
        onPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra(Constants.NEWS_ITEM_POSITION_KEY, position);
                resultIntent.putExtra(Constants.NEWS_LIST_TRANSPORT_KEY, state);
                setResult(RESULT_OK, resultIntent);
                presenter.setItemPosition(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };

        viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.addOnPageChangeListener(onPageChangeListener);
        newsPagerAdapter = new NewsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(newsPagerAdapter);
    }
}

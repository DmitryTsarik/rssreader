package com.raphanum.rssreader.model.dto;

public class ChannelDTO {
    private String title;
    private String url;

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }
}

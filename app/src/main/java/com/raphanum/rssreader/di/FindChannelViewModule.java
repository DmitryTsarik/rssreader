package com.raphanum.rssreader.di;

import com.raphanum.rssreader.presenter.FindChannelPresenter;
import com.raphanum.rssreader.view.FindChannelView;

import dagger.Module;
import dagger.Provides;

@Module
public class FindChannelViewModule {
    private FindChannelView view;

    public FindChannelViewModule(FindChannelView view) {
        this.view = view;
    }

    @Provides
    FindChannelPresenter provideFindChannelPresenter() {
        return new FindChannelPresenter(view);
    }
}

package com.raphanum.rssreader.model;

import com.raphanum.rssreader.model.realm.Channel;
import com.raphanum.rssreader.model.realm.RssItem;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observable;
import rx.functions.Func2;

public class ModelImpl implements Model {
    private Realm realm;

    public ModelImpl() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public Observable<List<RssItem>> getAllItems() {
        return realm.where(RssItem.class).findAll().asObservable()
                .flatMap(rssItems -> Observable.from(rssItems)
                        .take(rssItems.size())
                        .toSortedList(new Func2<RssItem, RssItem, Integer>() { //TODO: refactor this
                            @Override
                            public Integer call(RssItem rssItem, RssItem rssItem2) {
                                return rssItem2.getPubDate().compareTo(rssItem.getPubDate());
                            }
                        }));
    }

    @Override
    public Observable<List<RssItem>> getFavorites() {
        return realm.where(RssItem.class).findAll().asObservable()
                .flatMap(rssItems -> Observable.from(rssItems)
                        .filter(RssItem::isFavorite)
                        .take(rssItems.size())
                        .toSortedList(new Func2<RssItem, RssItem, Integer>() { //TODO: refactor this
                            @Override
                            public Integer call(RssItem rssItem, RssItem rssItem2) {
                                return rssItem2.getPubDate().compareTo(rssItem.getPubDate());
                            }
                        }));
    }

    @Override
    public Observable<List<Channel>> getChannelList() {
        RealmResults<Channel> results = realm.where(Channel.class).findAll();
        return results.asObservable()
                .flatMap(Observable::from)
                .take(results.size())
                .toList();
    }

    @Override
    public Observable<List<Channel>> getFakeChannelList() {
        return Observable.from(getFakeData())
                .toList();
    }

    private List<Channel> getFakeData() {
        List<Channel> list = new ArrayList<>();
        Channel lenta = new Channel();
        lenta.setUrl("https://lenta.ru/rss/");
        Channel bbc = new Channel();
        bbc.setUrl("http://feeds.bbci.co.uk/russian/rss.xml");
        list.add(lenta);
        list.add(bbc);
        return list;
    }
}

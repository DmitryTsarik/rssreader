package com.raphanum.rssreader.presenter;

import com.raphanum.rssreader.MyApplication;
import com.raphanum.rssreader.model.Model;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public abstract class BasePresenter implements Presenter {
    @Inject
    protected Model model;
    @Inject
    protected CompositeSubscription compositeSubscription;

    public BasePresenter() {
        MyApplication.getComponent().inject(this);
    }

    protected void addSubscription(Subscription subscription) {
        compositeSubscription.add(subscription);
    }

    protected void setSubscription(Subscription subscription) {
        compositeSubscription.clear();
        addSubscription(subscription);
    }

    @Override
    public void onDestroy() {
        compositeSubscription.clear();
    }
}

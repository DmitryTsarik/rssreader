package com.raphanum.rssreader.util;

import com.raphanum.rssreader.model.realm.Channel;
import com.raphanum.rssreader.model.realm.RssItem;

import java.util.List;

import io.realm.Realm;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

public class DatabaseManager {

    private Realm realm;

    public DatabaseManager() {
        realm = Realm.getDefaultInstance();
    }

    public void save(List<RssItem> list) {
        if (list.isEmpty()) {
            return;
        }
        realm.executeTransaction(transaction -> {
            for (RssItem item : list) {
                try {
                    realm.copyToRealm(item);
                } catch (RealmPrimaryKeyConstraintException e) {
                }
            }
        });
    }

    public void save(Channel channel) {
        realm.executeTransaction(transaction -> realm.copyToRealmOrUpdate(channel));
    }

    public void updateFavorite(RssItem item, boolean favorite) {
        realm.executeTransaction(transaction -> item.setFavorite(favorite));
    }

    public void deleteAllNews() {
        realm.beginTransaction();
        realm.delete(RssItem.class);
        realm.commitTransaction();
    }

    public void deleteAll() {
        realm.beginTransaction();
        realm.deleteAll();
        realm.delete(Channel.class);
        realm.commitTransaction();
    }
}

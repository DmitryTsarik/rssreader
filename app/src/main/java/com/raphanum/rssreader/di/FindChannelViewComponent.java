package com.raphanum.rssreader.di;

import com.raphanum.rssreader.view.fragments.FindTabFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {FindChannelViewModule.class, })
public interface FindChannelViewComponent {

    void inject(FindTabFragment fragment);
}

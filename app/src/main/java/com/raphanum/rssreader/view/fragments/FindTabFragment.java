package com.raphanum.rssreader.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.raphanum.rssreader.R;
import com.raphanum.rssreader.di.DaggerFindChannelViewComponent;
import com.raphanum.rssreader.di.FindChannelViewComponent;
import com.raphanum.rssreader.di.FindChannelViewModule;
import com.raphanum.rssreader.model.dto.ChannelDTO;
import com.raphanum.rssreader.presenter.FindChannelPresenter;
import com.raphanum.rssreader.view.FindChannelView;
import com.raphanum.rssreader.view.adpters.ChannelListAdapter;

import java.util.List;

import javax.inject.Inject;

public class FindTabFragment extends AbstractTabFragment implements FindChannelView {

    private Button findButton;
    private EditText editText;
    private TextWatcher textWatcher;
    private RecyclerView recyclerView;
    private ChannelListAdapter adapter;
    private FindChannelViewComponent viewComponent;
    private ProgressBar progressBar;
    @Inject
    protected FindChannelPresenter presenter;

    public static FindTabFragment newInstance(Context context) {
        FindTabFragment fragment = new FindTabFragment();
        fragment.setTitle(context.getString(R.string.tab_title_find));

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (viewComponent == null) {
            viewComponent = DaggerFindChannelViewComponent.builder()
                    .findChannelViewModule(new FindChannelViewModule(this))
                    .build();
        }
        viewComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_find_tab, container, false);
        editText = (EditText) fragmentView.findViewById(R.id.find_channels_edit_text);
        findButton = (Button) fragmentView.findViewById(R.id.find_channels_button);
        progressBar = (ProgressBar) fragmentView.findViewById(R.id.find_channels_progress);
        recyclerView = (RecyclerView) fragmentView.findViewById(R.id.find_channels_recycler_view);
        adapter = new ChannelListAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        findButton.setEnabled(false);
        initTextWatcher();
        editText.addTextChangedListener(textWatcher);
        findButton.setOnClickListener(view -> {
            hideKeyboard();
            presenter.onFindClick(editText.getText().toString());
        });
        return fragmentView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        editText.removeTextChangedListener(textWatcher);
    }

    private void initTextWatcher() {
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                findButton.setEnabled(charSequence.length() > 0);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }

    @Override
    public void showChannelList(List<ChannelDTO> list) {
        recyclerView.setVisibility(View.VISIBLE);
        adapter.setChannelList(list);
    }

    @Override
    public void showNotFound() {
        recyclerView.setVisibility(View.GONE);
        Toast.makeText(getContext(), "Not found", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) editText.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }
}

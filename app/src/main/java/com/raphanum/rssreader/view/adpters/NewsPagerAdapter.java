package com.raphanum.rssreader.view.adpters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.raphanum.rssreader.R;
import com.raphanum.rssreader.model.realm.RssItem;
import com.raphanum.rssreader.view.fragments.NewsFragment;

import java.util.ArrayList;
import java.util.List;

public class NewsPagerAdapter extends FragmentStatePagerAdapter {

    private List<RssItem> items;

    public NewsPagerAdapter(FragmentManager fm) {
        super(fm);
        items = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return NewsFragment.newInstance(items.get(position), (view, url) -> {
            GlideDrawableImageViewTarget target = new GlideDrawableImageViewTarget(view);
            Glide.with(view.getContext())
                    .load(url)
                    .placeholder(R.mipmap.feed)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(target);
        });
    }

    @Override
    public int getCount() {
        if (items != null) {
            return items.size();
        }
        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return position + "/" + getCount();
    }

    public void setItems(List<RssItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }
}

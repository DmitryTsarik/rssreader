package com.raphanum.rssreader.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.raphanum.rssreader.R;
import com.raphanum.rssreader.model.realm.RssItem;
import com.raphanum.rssreader.util.Constants;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class NewsFragment extends Fragment {
    private static final String CHANNEL_TITLE = "channelTitle";
    private static OnLoadImageListener onLoadImageListener;

    public NewsFragment() {}

    public static NewsFragment newInstance(RssItem item, OnLoadImageListener listener) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putString(Constants.RSS_TITLE_TAG, item.getTitle());
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DISPLAY_PUB_DATE_FORMAT, Locale.getDefault());
        args.putString(Constants.RSS_PUB_DATE_TAG, dateFormat.format(item.getPubDate()));
        args.putString(Constants.RSS_IMAGE_TAG, item.getChannelImageUrl());
        args.putString(CHANNEL_TITLE, item.getChannelTitle());
        args.putString(Constants.RSS_DESCRIPTION_TAG, item.getDescription());
        fragment.setArguments(args);
        onLoadImageListener = listener;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_news, container, false);
        ((TextView)rootView.findViewById(R.id.news_title)).setText(getArguments().getString(Constants.RSS_TITLE_TAG));
        ((TextView)rootView.findViewById(R.id.news_description)).setText(getArguments().getString(Constants.RSS_DESCRIPTION_TAG));
        ((TextView)rootView.findViewById(R.id.news_pub_date)).setText(getArguments().getString(Constants.RSS_PUB_DATE_TAG));
        ImageView imageView = ((ImageView)rootView.findViewById(R.id.news_channel_icon));
        onLoadImageListener.onLoad(imageView, getArguments().getString(Constants.RSS_IMAGE_TAG));
        ((TextView)rootView.findViewById(R.id.news_channel_title)).setText(getArguments().getString(CHANNEL_TITLE));
        return rootView;
    }

    public interface OnLoadImageListener {
        void onLoad(ImageView view, String url);
    }
}
package com.raphanum.rssreader.model;

import com.raphanum.rssreader.model.realm.Channel;
import com.raphanum.rssreader.model.realm.RssItem;

import java.util.List;

import rx.Observable;

public interface Model {
    Observable<List<RssItem>> getAllItems();

    Observable<List<RssItem>> getFavorites();

    //Observable<Channel> getChannelByUrl(String url);

    Observable<List<Channel>> getChannelList();

    Observable<List<Channel>> getFakeChannelList();
}
